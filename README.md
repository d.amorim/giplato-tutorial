# Master GI accelerator physics tutorial

Repository for the accelerator physics notebooks used in the pratical works of 2021-01-12. 

## Local installation
To run the notebooks we need
- A recent Python version (>3.6)
- Numerical computing and plotting packages (numpy, scipy, matplotlib, ipython)
- The cpymad package (https://pypi.org/project/cpymad/)

Anaconda an easy way to install the required elements. It is a scientific python distribution, which includes many packages for numerical computing, machine learning, data visualisation...
It is available for Windows, MacOS and Linux at https://www.anaconda.com/products/individual
Once installed, the Anaconda Navigator can be executed. There one can launch a Jupyter Notebook.
Detailled instructions are given at https://docs.anaconda.com/anaconda/user-guide/getting-started/.

The first notebook ```tp_accelerator_physics_1``` contains a cell to run which will install the ```cpymad``` package if not already available.

## Online plateforms
The notebooks can be deployed using binder https://mybinder.org/ under this link [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/d.amorim%2Fgiplato-tutorial/HEAD?urlpath=lab)

They also have been tested on Google Collaboratory (https://colab.research.google.com).
Google Collaboratory includes most of the required packages, the ```cpymad``` package will be installed on the fly when running the first cell.